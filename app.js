const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const ejs = require('ejs');
const path= require('path');
const cookieParser = require('cookie-parser');
require('dotenv').config();
const app = express();

const router = express.Router();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');   
app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(session({ //will work with everything below this so declare your static assets above
  secret: 'cadenza',//config.SESSION_SECRET,
  resave: false,
  saveUninitialized: false,
}));


app.use('/',router);
  router.use((req, res, next)=> {
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
      res.locals.error = process.env.NODE_ENV ==='production'? err : {};
      next();
  });

const store= require('./routes/routes');
const product= require('./routes/product');
const subscription= require('./routes/subscription');
const plan= require('./routes/plan');
const payment=require('./routes/payment');
const algorithm=require('./routes/algorithm');

app.use('/',store);
app.use('/',product);
app.use('/',subscription);
app.use('/',plan);
app.use('/',payment);
app.use('/',algorithm);
module.exports = app;


