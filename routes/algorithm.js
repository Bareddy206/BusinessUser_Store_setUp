const router = require('express').Router();
const seneca = require('seneca')();


router.post('/addquestion', (req, res) => {
	var token = req.body.token || req.headers['token'];
	var bodyParams = req.body;
	bodyParams.token = token;
	seneca.client({
			//host:'ec2-13-127-16-45.ap-south-1.compute.amazonaws.com',
			host:'localhost',
			port:2750,
			timeout$: 999999
		})
		.act({
			"role": "algorithm",
			"cmd": "addQuestion",
			"bodyParams": bodyParams
		}, (err, reply) => {
			if (err) {
				//console.error("error while updating basket with plans:"+ err);
				return res.send({
					status: 'error',
					message: 'Client request timeout!'
				})
			} else {
				//console.log("algorithm added:"+ JSON.stringify(reply));
				return res.send(reply.algorithm);
			}
		});
});
router.post('/updateAlgorithm', (req, res) => {
	var token = req.body.token || req.headers['token'];
	var bodyParams = req.body;
	bodyParams.token = token;
	seneca.client({
			//host: 'ec2-13-127-16-45.ap-south-1.compute.amazonaws.com',
			host:'localhost',
			port: 2754,
			timeout$: 999999
		})
		.act({
			"role": "algorithm",
			"cmd": "updateQuestion",
			"bodyParams": bodyParams
		}, (err, reply) => {
			if (err) {
				//console.error("error while updating basket with plans:"+ err);
				return res.send({
					status: 'error',
					message: 'Client request timeout!'
				})
			} else {
				//console.log("updated  algorithm with suggesions:"+ JSON.stringify(reply));
				return res.send(reply);
			}
		});
});
module.exports=router;