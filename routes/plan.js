const router = require('express').Router();
const seneca = require('seneca')();




//update basket with plans
router.post('/addPlans', (req, res) => {
	var token = req.body.token || req.headers['token'];
	var bodyParams = req.body;
	bodyParams.token = token;
	//console.log("addPlans::" + bodyParams.token);
	seneca.client({
			host: 'ec2-13-127-37-110.ap-south-1.compute.amazonaws.com',
			//host:'localhost',
			port: 2654,
			timeout$: 999999
		})
		.act({
			"role": "subscription",
			"cmd": "updatewithPlans",
			"bodyParams": bodyParams
		}, (err, reply) => {
			if (err) {
				console.error("error while updating basket with plans:" + err);
				res.send({
					status: 'error',
					message: 'Client request timeout!'
				})
			} else {
				//console.log("updated  basket with plans:"+ JSON.stringify(reply));
				res.send(reply);
			}
		});
});

module.exports = router;