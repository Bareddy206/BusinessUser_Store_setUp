const router = require('express').Router();
const jwt = require('jsonwebtoken');
var multer = require('multer');
const seneca = require('seneca')();
const fs = require('fs');
//var session = require('express-session');


/* Multer set storage location*/
var storage = multer.diskStorage({

	filename: function (req, file, cb) {
		var str = file.originalname;
		str = str.replace(/\s+/g, '-').toLowerCase();
		global.poster = Date.now() + '_' + str;
		cb(null, poster);
	}
});
var upload = multer({
	storage: storage
});

router.post('/addBasket', upload.single('image'), (req, res) => {
	var token = req.body.token || req.headers['token'];
	var bodyParams = req.body;
	bodyParams.poster = poster;
	//bodyParams.imagepath=req.file.path;
	const base64Data = new Buffer(fs.readFileSync(req.file.path)).toString("base64");
	bodyParams['base64Data'] = base64Data;
	bodyParams.token = token;
	seneca.client({
			//host: 'ec2-13-127-37-110.ap-south-1.compute.amazonaws.com',
			host:'localhost',
			port: 2650,
			timeout$:999999
		})
		.act({
			"role": "subscription",
			"cmd": "addSubscription",
			"bodyParams": bodyParams
		}, (err, reply) => {
			if (err) {
				console.error("error while adding basket:" + err);
				return res.send({
					status: 'error',
					message: 'Client request timeout!'
				})
			} else {
				//console.log("added basket:" + JSON.stringify(reply));
				return res.send(reply);
			}
		});


});
router.get('/getallSubscriptions', (req, res) => {
	var token = req.body.token || req.headers['token'];
	var bodyParams = req.body;
	seneca.client({
			//host: 'ec2-13-127-37-110.ap-south-1.compute.amazonaws.com',
			host:'localhost',
			port: 2658,
			timeout$: 999999
		})
		.act({
			"role": "subscription",
			"cmd": "getallSubscriptions",
			"token": token
		}, (err, reply) => {
			if (err) {
				console.error("error while getting baskets:" + err);
				res.send({
					status: 'error',
					message: 'Client request timeout!'
				})
			} else {
				res.send(reply.baskets);
			}
		});

});
router.post('/getSubscription', (req, res) => {
	var token = req.body.token || req.headers['token'];
	var bodyParams = req.body;
	seneca.client({
			host:'ec2-13-127-37-110.ap-south-1.compute.amazonaws.com',
			port:2656,
			timeout$: 999999
		})
		.act({
			"role": "subscription",
			"cmd": "getSubscription",
			"bodyParams":bodyParams
		}, (err, reply) => {
			if (err) {
				console.error("error while getting basket:" + err);
				res.send({
					status: 'error',
					message: 'Client request timeout!'
				})
			} else {
				res.send(reply.basket);
			}
		});

});
//update Product with seneca
router.post('/updateSubscription', upload.single('image'), (req, res) => {
	var token = req.body.token || req.headers['token'];
	console.log("in updatesubscriptionfunction::"+ token);
	var bodyParams = req.body;
	bodyParams['token'] = token;
	if (req.file == undefined) {
		seneca.client({
				host:'ec2-13-127-37-110.ap-south-1.compute.amazonaws.com',
				port:2654,
				timeout$:999999
			})
			.act({
				"role":"subscription",
				"cmd": "updateSubscription",
				"bodyParams":bodyParams,
			}, (err, reply) => {
				if (err) {
					console.log("error while updating subscription:" + err);
					res.send({
						status: 'error',
						message: 'Client request timeout!'
					})
				} else {
					//console.log("added product:"+ JSON.stringify(reply));
					 res.send({
						status: reply.status,
						message: reply.message
					});
				}
			});
	} else {
		const base64Data = new Buffer(fs.readFileSync(req.file.path)).toString("base64");
		bodyParams['base64Data'] = base64Data;
		bodyParams['poster'] = poster;
		seneca.client({
				host:'ec2-13-127-37-110.ap-south-1.compute.amazonaws.com',
				port:2654,
				timeout$:999999
			})
			.act({
				"role": "subscription",
				"cmd": "updateSubscription",
				"bodyParams": bodyParams,
			}, (err, reply) => {
				if (err) {
					console.error("error while update subscription:" + err);
					return res.send({
						status: 'error',
						message: 'Client request timeout!'
					})
				} else {
					//console.log("added product:"+ JSON.stringify(reply));
					res.send({
						status: reply.status,
						message: reply.message
					});
				}
			});


	}
});


module.exports = router;