const router = require('express').Router();
const jwt = require('jsonwebtoken');
var multer = require('multer');
const seneca = require('seneca')();
const fs = require('fs');
//var session = require('express-session');


/* Multer set storage location*/
var storage = multer.diskStorage({

	filename: function (req, file, cb) {
		var str = file.originalname;
		str = str.replace(/\s+/g, '-').toLowerCase();
		global.poster = Date.now() + '_' + str;
		cb(null, poster);
	}
});
var upload = multer({
	storage: storage
});

//post data to microservices
//add product with seneca
router.post('/addproduct', upload.single('image'), (req, res) => {
	var token = req.body.token || req.headers['token'];
	var bodyParams = req.body;
	//console.log(" in addProduct::"+ JSON.stringify(bodyParams));
	const base64Data = new Buffer(fs.readFileSync(req.file.path)).toString("base64");
	bodyParams['base64Data'] = base64Data;
	bodyParams['poster'] = poster;
	bodyParams['token'] = token;
	seneca.client({
			//host:'ec2-13-127-198-89.ap-south-1.compute.amazonaws.com',
			host:'localhost',
			port:2450,
			timeout$:999999
		})
		.act({
			"role": "product",
			"cmd": "addProduct",
			"bodyParams": bodyParams
		}, (err, reply) => {
			if (err) {
				//console.error("error while adding product:" + err);
				res.send({
					status: 'error',
					message: 'Client request timeout!'
				})
			} else {
				//console.log("added product:" + JSON.stringify(reply));
				res.send(reply);
			}
		});



});
//getProducts with seneca
router.get('/getProducts', (req, res) => {
	var token = req.body.token || req.headers['token'];
	//console.log("suma");
	seneca.client({
			//host:'ec2-13-127-198-89.ap-south-1.compute.amazonaws.com',
			host:'localhost',
			port:2458,
			timeout$:999999
		})
		.act({
			"role": "product",
			"cmd": "getProducts",
			"token": token
		}, (err, reply) => {
			if (err) {
				console.log("error while getting products:" + err);
				 res.send({
					status: 'error',
					message: 'Client request timeout!'
				})
			} else {
				//console.log("product Details:" + JSON.stringify(reply.products));
				 res.send(reply.products);
			}
		});

});
router.post('/getProduct', (req, res) => {
	var token = req.body.token || req.headers['token'];
	//console.log("suma");
	var bodyParams=req.body;
	console.log("in get Product",JSON.stringify(bodyParams));
	seneca.client({
			//host:'ec2-13-127-198-89.ap-south-1.compute.amazonaws.com',
			host:'localhost',
			port:2452,
			timeout$:999999
		})
		.act({
			"role": "product",
			"cmd": "getProduct",
			"bodyParams": bodyParams
		}, (err, reply) => {
			if (err) {
				console.log("error while getting products:" + err);
				 res.send({
					status: 'error',
					message: 'Client request timeout!'
				})
			} else {
				//console.log("product Details:" + JSON.stringify(reply.product));
				 res.send(reply.product);
			}
		});

});
//update Product with seneca
router.post('/update', upload.single('image'), (req, res) => {
	var token = req.body.token || req.headers['token'];
	//console.log("in updateProduct function::"+ token);
	var bodyParams = req.body;
	bodyParams['token'] = token;
	if (req.file == undefined) {
		seneca.client({
				//host:'ec2-13-127-198-89.ap-south-1.compute.amazonaws.com',
				host:'localhost',
				port:2456,
				timeout$:999999
			})
			.act({
				"role":"product",
				"cmd": "updateProduct",
				"bodyParams":bodyParams,
			}, (err, reply) => {
				if (err) {
					console.log("error while updating products:" + err);
					res.send({
						status: 'error',
						message: 'Client request timeout!'
					})
				} else {
					//console.log("added product:"+ JSON.stringify(reply));
					 res.send({
						status: reply.status,
						message: reply.message
					});
				}
			});
	} else {
		const base64Data = new Buffer(fs.readFileSync(req.file.path)).toString("base64");
		bodyParams['base64Data'] = base64Data;
		bodyParams['poster'] = poster;
		seneca.client({
				//host:'ec2-13-127-198-89.ap-south-1.compute.amazonaws.com',
				host:'localhost',
				port:2456,
				timeout$:999999
			})
			.act({
				"role": "product",
				"cmd": "updateProduct",
				"bodyParams": bodyParams,
			}, (err, reply) => {
				if (err) {
					console.error("error while update product:" + err);
					return res.send({
						status: 'error',
						message: 'Client request timeout!'
					})
				} else {
					//console.log("added product:"+ JSON.stringify(reply));
					res.send({
						status: reply.status,
						message: reply.message
					});
				}
			});


	}
});
module.exports = router;