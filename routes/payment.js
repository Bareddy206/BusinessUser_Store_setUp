const router = require('express').Router();
var multer = require('multer');
const seneca = require('seneca')();
const fs = require('fs');

/* Multer set storage location*/
var storage = multer.diskStorage({

	filename: function (req, file, cb) {
		var str = file.originalname;
		str = str.replace(/\s+/g, '-').toLowerCase();
		global.poster = Date.now() + '_' + str;
		cb(null, poster);
	}
});
var upload = multer({
	storage: storage
});
router.post('/uploadPDF',upload.single('pdf'),(req,res)=>{
console.log("filePath"+req.file.path);	
console.log("poster"+ poster);
// for (const file of req.files) {
// 	console.log(file.filename)
//   }
//var token = req.body.token || req.headers['token'];
	var bodyParams = req.body;
	const base64Data = new Buffer(fs.readFileSync(req.file.path)).toString('base64');
	bodyParams['base64Data'] = base64Data;
	bodyParams['poster']=poster;
	seneca.client({
			//host: 'ec2-13-127-232-53.ap-south-1.compute.amazonaws.com',
			host:'localhost',
			port: 2557,
			timeout$: 999999999
		})
		.act({
			"role": "user",
			"cmd": "uploadPdf",
			"bodyParams": bodyParams
		}, (err, reply) => {
			if (err) {
				console.error("error while uploading pdf:" + err);
				return res.send({
					status: 'error',
					message: 'Client request timeout!'
				})
			} else {
				//console.log(" account setup:" + JSON.stringify(reply));
				return res.send(reply);
			}
		});
});



router.post('/addAccount', (req, res) => {
	var token = req.body.token || req.headers['token'];
	var bodyParams = req.body;
	bodyParams.token = token;
	seneca.client({
			//host: 'ec2-13-127-232-53.ap-south-1.compute.amazonaws.com',
			host:'localhost',
			port: 2557,
			timeout$: 999999
		})
		.act({
			"role": "user",
			"cmd": "accountsetup",
			"bodyParams": bodyParams
		}, (err, reply) => {
			if (err) {
				console.error("error while setup the account:" + err);
				return res.send({
					status: 'error',
					message: 'Client request timeout!'
				})
			} else {
				//console.log(" account setup:" + JSON.stringify(reply));
				return res.send(reply);
			}
		});

});

module.exports=router;