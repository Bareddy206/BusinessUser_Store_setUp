const router = require('express').Router();
const jwt = require('jsonwebtoken');
const seneca = require('seneca')();
router.get('/', (req, res) => {
	var token = req.query.token;
	if (token) {
		jwt.verify(token, process.env.SECRET_KEY, (err, decode) => {
			if (err) {
				res.status(500).send({
					status: "failure",
					message: "Invalid Token"
				});

			} else {

				req.session.storeName = decode.storeName;
				req.session.token = token;
				console.log("step::"+decode.step);
				if(decode.step=='product'){res.redirect('/product');}
				else if(decode.step==='subscription'){res.redirect('/subscription');}
				else if(decode.step==='plan'){res.redirect('/plan');}
				else if(decode.step==='payment'){res.redirect('/payment');}
				else if(decode.step==='algorithm'){res.redirect('/algorithm');}
				else if(decode.step==='dashboard'){res.redirect("http://dashboard.cadenza.tech/?token="+token);}
				else{}
			}
		});
	} else {
		res.redirect("https://admin.cadenza.tech");
	}


});
router.get('/product', (req, res) => {
	var token=req.session.token;
	if(token){
		seneca.client({
			host:'ec2-13-232-92-165.ap-south-1.compute.amazonaws.com',
			//host:'localhost',
			port:2554,
			timeout$:999999
		})
		.act({
			"role": "user",
			"cmd": "getSession",
			"token": token
		}, (err, reply) => {
			if (err) {
				console.log("error::"+err);
			} else {
				//console.log("data::"+JSON.stringify(reply.steps_completed));
				if(reply.steps_completed.steptocomplete=='product'){
					res.locals.SKU = Date.now();
					res.locals.storeName = req.session.storeName;
					res.locals.token =req.session.token
					//console.log("product"+req.session.storeName);
					res.render('store_setup_product');
				}else if(reply.steps_completed.steptocomplete=='subscription'){res.redirect('/subscription');}
				else if(reply.steps_completed.steptocomplete=='plan'){res.redirect('/plan');}
				else if(reply.steps_completed.steptocomplete==='payment'){res.redirect('/payment');}
				else if(reply.steps_completed.steptocomplete==='5'){res.redirect('/algorithm');}
				else if(reply.steps_completed.steptocomplete==='dashboard'){res.redirect("https://dashboard.cadenza.tech/?token="+token);}
				else{}		
					
			}
		});

	}else{
		res.redirect("https://admin.cadenza.tech");
	}
	
	
			
				
});
router.get('/subscription', (req, res) => {
	var token=req.session.token;
	//console.log("token:"+token);
	console.log("subscription::");
	if(token){
	seneca.client({
		host:'ec2-13-232-92-165.ap-south-1.compute.amazonaws.com',
		//host:'localhost',
		port:2554,
		timeout$:999999
	})
	.act({
		"role": "user",
		"cmd": "getSession",
		"token": token
	}, (err, reply) => {
		if (err) {
			console.log("error::"+err);
		} else {
			//console.log("data::"+JSON.stringify(reply.steps_completed));
			if(reply.steps_completed.steptocomplete=='subscription'){
				res.locals.token =req.session.token;
				res.locals.SKU = Date.now();
				//console.log("token:: in sub"+res.locals.token);
				res.render('store_setup_subscription');
			}
			else if(reply.steps_completed.steptocomplete==='product'){res.redirect('/product');}
			else if(reply.steps_completed.steptocomplete==='plan'){res.redirect('/plan');}
			else if(reply.steps_completed.steptocomplete==='payment'){res.redirect('/payment');}
			else if(reply.steps_completed.steptocomplete==='algorithm'){res.redirect('/algorithm');}
			else if(reply.steps_completed.steptocomplete==='dashboard'){res.redirect("https://dashboard.cadenza.tech/?token="+token);}
			else{}	
				
		}
	});}
	else{
		res.redirect("https://admin.cadenza.tech");
	}		
});

router.get('/plan', (req, res) => {
	var token=req.session.token;
	if(token){
	seneca.client({
		host:'ec2-13-232-92-165.ap-south-1.compute.amazonaws.com',
		//host:'localhost',
		port:2554,
		timeout$:999999
	})
	.act({
		"role": "user",
		"cmd": "getSession",
		"token": token
	}, (err, reply) => {
		if (err) {
			console.log("error::"+err);
		} else {
			//console.log("data::"+JSON.stringify(reply.steps_completed));
			if(reply.steps_completed.steptocomplete==='plan'){
				res.locals.token = req.session.token;
				var subscription_ids=reply.steps_completed.subscription_ids;
				res.locals.subscriptionSKU=subscription_ids[subscription_ids.length-1];
				console.log("subscriptionSKU::"+res.subscriptionSKU);
				res.render('store_setup_plan');
			}else if(reply.steps_completed.steptocomplete==='subscription'){res.redirect('/subscription');}
			else if(reply.steps_completed.steptocomplete==='product'){res.redirect('/product');}
			else if(reply.steps_completed.steptocomplete==='payment'){res.redirect('/payment');}
			else if(reply.steps_completed.steptocomplete==='algorithm'){res.redirect('/algorithm');}
			else if(reply.steps_completed.steptocomplete==='dashboard'){res.redirect("https://dashboard.cadenza.tech/?token="+token);}
			else{}	
		}
	});}
	else{
		res.redirect("https://admin.cadenza.tech");
	}
});

router.get('/payment', (req, res) => {
	var token=req.session.token;
	if(token){
	seneca.client({
		host:'ec2-13-232-92-165.ap-south-1.compute.amazonaws.com',
		//port:2557,
		//host:'localhost',
		port:2554,
		timeout$:999999
	})
	.act({
		"role": "user",
		"cmd": "getSession",
		"token": token
	}, (err, reply) => {
		if (err) {
			console.log("error::"+err);
		} else {
			//console.log("data::"+JSON.stringify(reply.steps_completed));
			if(reply.steps_completed.steptocomplete==='payment'){
				res.locals.token = req.session.token;
				res.render('store_setup_payment');
			}else if(reply.steps_completed.steptocomplete=='subscription'){res.redirect('/subscription');}
			else if(reply.steps_completed.steptocomplete==='product'){res.redirect('/product');}
			else if(reply.steps_completed.steptocomplete==='plan'){res.redirect('/plan');}
			else if(reply.steps_completed.steptocomplete==='algorithm'){res.redirect('/algorithm');}
			else if(reply.steps_completed.steptocomplete==='dashboard'){res.redirect("https://dashboard.cadenza.tech/?token="+token);}
			else{}
		}
	});
	}else{
		res.redirect("https://admin.cadenza.tech");
	}
	
	
	
});
router.get('/algorithm', (req, res) => {
	var token=req.session.token;
	if(token){
	seneca.client({
		host:'ec2-13-232-92-165.ap-south-1.compute.amazonaws.com',
		// port:2557,
		host:'localhost',
		port:2554,
		timeout$:999999
	})
	.act({
		"role": "user",
		"cmd": "getSession",
		"token": token
	}, (err, reply) => {
		if (err) {
			console.log("error::"+err);
		} else {
			//console.log("data::"+JSON.stringify(reply.steps_completed));
			if(reply.steps_completed.steptocomplete==='algorithm'){
				res.locals.token =req.session.token;
				res.render('algorithm');
			}else if(reply.steps_completed.steptocomplete==='subscription'){res.redirect('/subscription');}
			else if(reply.steps_completed.steptocomplete==='product'){res.redirect('/product');}
			else if(reply.steps_completed.steptocomplete==='payment'){res.redirect('/payment');}
			else if(reply.steps_completed.steptocomplete==='plan'){res.redirect('/plan');}
			else if(reply.steps_completed.steptocomplete==='dashboard'){res.redirect("https://dashboard.cadenza.tech/?token="+token);}
			else{}
		}
	});}
	else{
		res.redirect("https://admin.cadenza.tech");
	}
});
router.post('/updateSession',(req,res)=>{
	var token=req.body.token;
	var step=req.body.step;
	//console.log("update Session::\n"+token);
	//console.log("update Session::\n"+step);
	seneca.client({
		host:'ec2-13-127-198-89.ap-south-1.compute.amazonaws.com',
		//host:'localhost',
		port:2450,
		timeout$:999999
	})
	.act({
		"role": "product",
		"cmd": "updateSession",
		"token": token,
		"step":step
	}, (err, reply) => {
		if (err) {
			//console.error("error while updating session with completed steps:" + err);
			res.send({
				status: 'error',
				message: 'Client request timeout!'
			})
		} else {
			console.log("update Session:" + JSON.stringify(reply));
			res.send(reply);
		}
	});

});
module.exports = router;